import styled, {keyframes} from 'styled-components';

export const Container = styled.div`
  padding: 150px;
  margin: 0px auto;
  width: 50%;
  display: flex;
  justify-content: center;
`

const showAnimtion = keyframes`
  0% {
    opacity: 0 
  }
  50% {
    opacity: 0.6
  }
  100% {
    opacity: 1
  }
`

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    animation: ${showAnimtion} 3s ease-in;
    box-shadow: -2px 4px 5px 0px rgba(0,0,0,0.25);
    margin: 0 10px;
    padding: 10px;
`