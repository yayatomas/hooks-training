import React, {useContext, useLayoutEffect, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {BouquetesContext} from '../../provider/BouquetesProvider';
import Loader from '../../components/Loader/Loader';
import {Container, Content} from './styles';
const BouqueteDetail = () => {
  const {
    getBouquete,
    loading, 
    state: {
      bouquete: {
        name,
        price,
        image
      }
    }
  } = useContext(BouquetesContext);

  const {id} = useParams();

  useEffect(() => { 
    getBouquete(id) }, [getBouquete, id]);

    console.log(loading)
    
  return(
   <Container>
     {
       loading ? <Loader /> :
        <Content>
          <img src={image} alt={name}/>
          <h3>{name}</h3>
          <p>{price}</p>
        </Content>
     }
   </Container>
  )
}

export default BouqueteDetail;

