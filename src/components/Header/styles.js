import styled from 'styled-components';

export const Container = styled.div`
    background: #D8D8D8; 
    height: 50px;
    width: 100%;
    z-index: 40;
    position: fixed;
    margin-top: -20px;
    padding-top: 30px;
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.28);
`