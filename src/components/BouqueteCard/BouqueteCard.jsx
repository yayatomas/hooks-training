import React from 'react';
import {Link} from 'react-router-dom';
import {useLazyLoading} from '../../hooks/useLazyLoading';
import {
    Container, 
    Wrapper, 
    Image, 
    Button,
    LinkText,
} from './styles';

const BouqueteCard = ({
    handleClick,
    price,
    image,
    name,
    id,
}) => {
    const [ show, element ] = useLazyLoading()
    return(
        <Wrapper ref={element}>
            {   
                show &&
                <Container>
                    <Button onClick={() => handleClick(id)}>Remove</Button>
                    <Image src={image} alt={name} />
                    <Link style={{textDecoration: 'none'}} to ={`/bouquete/${id}`}>
                        <LinkText>
                            {name}
                        </LinkText>
                    </Link>
                    <p>{price}</p>
                </Container>
            }
        </Wrapper>
    );
};

export default BouqueteCard;
