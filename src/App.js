import React, {Suspense, lazy} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import BouquetesProvider from './provider/BouquetesProvider';
import Home from './view/Home/Home';
import Loader from './components/Loader/Loader';
import Header from './components/Header/Header'; 
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
const BouqueteDetail = lazy(() => import('./view/BouqueteDetail/BouqueteDetail'))

function App() {
  return (
    <Router>
      <BouquetesProvider>
        <Suspense fallback={Loader}>
          <ErrorBoundary>
            <Header />
            <Switch>
              <Route path='/bouquete/:id' component={BouqueteDetail} />
              <Route exact path='/' component={Home} />
            </Switch>
          </ErrorBoundary>
        </Suspense>
      </BouquetesProvider>
    </Router>
  );
}

export default App;
